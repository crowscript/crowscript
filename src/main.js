import Vue from 'vue'
import App from './App.vue'

new Vue({
  el: '#app',
  render: h => h(App)
})

// setup typewriter effect in the terminal demo
if (document.getElementsByClassName('hero-terminal-code').length > 0) {
  var i = 0;
  var txt = `Hello there!
I’m Stan, a Frontend developer based in Düsseldorf, Germany.

frontend.expiriance = {
  skills: [
    html5: 'semantic', css: 'sass', javascript: 'vanilla'
  ],
  cities: [
    USA: 'Chicago', Bosnia: 'Banjaluka', Germany: 'Düsseldorf'
  ]
};

Welcome to my world of the coding.`;
  var speed = 60;

  function typeItOut () {
    if (i < txt.length) {
      document.getElementsByClassName('hero-terminal-code')[0].innerHTML += txt.charAt(i);
      i++;
      setTimeout(typeItOut, speed);
    }
  }

  setTimeout(typeItOut, 1800);
}
